A simple Integration of a Optirack NatNet client into Unity.

The optitrack client is based on:
https://bitbucket.org/jens_grubert/optitrack_test
However, this client (under optitrack-client-windows-static-functions) exposes all methods as static. This is required for access to the methods in Unity. 


The localip, serverip and the rigidbody id have to be edited inside the OptitrackTest.cs file under
UnityProject\simplescene\Assets\OptitrackTest.cs

The DLLs NatNetLib.dll  and OptitrackLibShared.dll have to be in the root directory of the Unity project (the folder above the "Assets" folder).
If you use 64-bit Unity, the files also have to be 64-bit.