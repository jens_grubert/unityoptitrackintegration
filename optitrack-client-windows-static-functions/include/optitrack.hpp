#ifndef _OPTITRACK_HPP_
#define _OPTITRACK_HPP_ 1

#define DLL_EXPORT __declspec(dllexport)

//#include <opencv2/core/core.hpp>
#include <stdio.h>
#include <tchar.h>
#include <conio.h>
#include <winsock2.h>

#include "NatNetTypes.h"
#include "NatNetClient.h"

#include <string>
#include <sstream>

#pragma warning( disable : 4996 )
using namespace std;

extern "C" {
	// declaring functions that can be used externally:
	DLL_EXPORT bool init(char* localip, char* serverip);
	DLL_EXPORT void enableRigidBodyTracking();
	DLL_EXPORT void enableSingleMarkerTracking();
	DLL_EXPORT bool newRigidBodyDataAvailableForID(int id);
	DLL_EXPORT bool newSingleMarkerDataAvailable();
	DLL_EXPORT void getLatestRigidBodyDataForID(char* sbptr, int id);
	DLL_EXPORT void getLatestSingleMarkerData(char* sbptr);
	DLL_EXPORT void closeConnection();

	class Optitrack {

	public:
		static bool init(string localip, string serverip); 
		static void enableRigidBodyTracking(); // track a rigid body? 
		static void disableRigidBodyTracking();
		static bool isRigidBodyTrackingEnabled();
		static void enableSingleMarkerTracking(); // track an individual marker -- assumes a single additional marker to be visible? 
		static void disableSingleMarkerTracking();
		static bool isSingleMarkerTrackingEnabled();

		//bool pollNewData(); // request new data explicitly, callback seems not to work
		//bool pollNewData2();
		

		static bool newRigidBodyDataAvailableForID(int id); 
		static bool newSingleMarkerDataAvailable(); 
		static string getLatestRigidBodyDataForID(int id);
		static string getLatestSingleMarkerData(); 

		static bool _newRigidBodyDataAvailableForID[];
		static bool _newSingleMarkerDataAvailable;
		static string _lastSingleMarkerData;


		static int _rigidBodyId; /// the rigidbody id we are interested in
		static bool _trackRigidBody;
		static bool _trackSingleMarker;

		static int* dataArray;
		static char* val;

		static string _rigidBodyDataStrings [];

	protected:


		static char szMyIPAddress[128];
		static char szServerIPAddress[128];
		static string _localIPStr;
		static string _serverIPStr;

		static int analogSamplesPerMocapFrame;

		//int analogSamplesPerMocapFrame;

		//bool _printVerbose; // should we printout info on the console

	};
}
#endif