﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Text;
#if UNITY_EDITOR
using UnityEditor;
#endif

// this script requires a unity scene with a cube named "cub" and a GUIText named "guitext".
// it is also required to put the libraries OptitrackLibShared.dll and NatNetLib.dll inside the root directory of the unity project
// (the folder above the "Assets" folder)
public class OptitrackTest : MonoBehaviour
{
    [DllImport("OptitrackLibShared.dll")]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool init(string localip, string serverip);
    [DllImport("OptitrackLibShared.dll")]
    private static extern void enableRigidBodyTracking(int rigidBodyId);
    [DllImport("OptitrackLibShared.dll")]
    private static extern void enableSingleMarkerTracking();
    [DllImport("OptitrackLibShared.dll")]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool newRigidBodyDataAvailable();
    [DllImport("OptitrackLibShared.dll")]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool newSingleMarkerDataAvailable();
    [DllImport("OptitrackLibShared.dll")]
    private static extern void getLatestRigidBodyData(StringBuilder sb);
    [DllImport("OptitrackLibShared.dll")]
    private static extern void getLatestSingleMarkerData(StringBuilder sb);
    [DllImport("OptitrackLibShared.dll")]
    private static extern void closeConnection();

    // local ip, server ip and rigid body id
    // this line has to be changed depending on the system you are executing this script
    string[] args = { "192.168.0.154", "192.168.0.161", "2" };

    // used for increased movement of the cube GameObject
    float multiplicator = 3f;

    #if UNITY_EDITOR
    // used to be able to close the connection when stopping the playmode in unity
    bool initLoadComplete = false;
    #endif
    

    void Start()
    {
        if (init(args[0], args[1]) == false)
        {
            print("could not initialize optitrack");
            return;
        }
        enableRigidBodyTracking(Int32.Parse(args[2]));
        //enableSingleMarkerTracking();

        #if UNITY_EDITOR
        EditorApplication.playmodeStateChanged = HandleOnPlayModeChanged;
        initLoadComplete = true;
        #endif
    }

    // Update is called once per frame
    void Update()
    {
        if (newRigidBodyDataAvailable())
        {
            StringBuilder sb = new StringBuilder(200);

            // write the current data into the stringbuilder
            getLatestRigidBodyData(sb);
            string receivedData = sb.ToString();
            float[] positions = getPositionDataFromString(receivedData);

            // print to unity console
            print("rb data: " + receivedData + Environment.NewLine);

            // set the displayed text
            GUIText txt = (GUIText)GameObject.Find("guitext").GetComponent("GUIText");
            txt.text = getTextForGUIText(positions);

            // set position and rotation of the cube
            GameObject.Find("cub").transform.position = new Vector3(multiplicator * positions[0], multiplicator * positions[1], -multiplicator * positions[2]);
            GameObject.Find("cub").transform.rotation = new Quaternion(multiplicator * positions[3], multiplicator * positions[4], -multiplicator * positions[5], -multiplicator * positions[6]);
        }
    }

#if UNITY_EDITOR
    // is called when the mode of the playmode changes. if playmode is stopped -> the connection is closed. 
    // if this is not present when executing the script in unity, the unity crashes at the second time
    void HandleOnPlayModeChanged()
    {
        if (initLoadComplete && !(EditorApplication.isPlaying || EditorApplication.isPlayingOrWillChangePlaymode))
        {
            // playmode in editor was stopped
            initLoadComplete = false;
            closeConnection();
        }
    }
#endif

    // creates a float[] containing the position and rotation values of the rigid body in this order:
    // X, Y, Z, QX, QY, QZ, QW
    float[] getPositionDataFromString(string datastring)
    {
        return Array.ConvertAll(datastring.Split(';'), new Converter<string, float>(float.Parse));
    }

    // builds the the displayed text containg the position and rotation values
    string getTextForGUIText(float[] positions)
    {
        return "X: " + positions[0] + Environment.NewLine + "Y: " + positions[1] + Environment.NewLine + "-Z: " + positions[2] + Environment.NewLine + "QX: "
                + positions[3] + Environment.NewLine + "-QY: " + positions[4] + Environment.NewLine + "-QZ: " + positions[5]
                + Environment.NewLine + "QW: " + positions[6] + Environment.NewLine;
    }

}
